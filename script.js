function Calcularlos() {
  var largo = document.getElementById("largo"),
    largo = parseFloat(largo.value).toFixed(2);
  var ancho = document.getElementById("ancho"),
    ancho = parseFloat(ancho.value).toFixed(2);
  area = (largo * ancho).toFixed(2);
  var alto = document.getElementById("alto"),
    alto = parseFloat(alto.value).toFixed(2),
    cubico = parseFloat(area * alto).toFixed(2);
  frigorias = parseFloat(cubico * 50).toFixed(2);
  resultado =
    "El area cubierta es de " +
    area +
    " metros cuadrados." +
    "<br><br>Los metros cubicos son: " +
    cubico +
    "." +
    "<br><br>Las frigorias necesarias para enfriar dicho espacio es de: " +
    frigorias +
    " como minimo.";
  document.getElementById("resultado").innerHTML = resultado;
}
